<?php

header("content-type:text/html;charset=utf-8");
	session_start();
	$userid=$_SESSION['account'];
	$app = [];
	$app['config'] = require '../../config.php'; 
	require '../../Core/DataBase/ConnetDB.php';
	require '../../Core/DataBase/RequestDB.php';
	require '../../Request/VideoRequest.php';
	
	$pdo = ConnetDB::make($app['config']['database']);
	$quesy = new RequestDB($pdo);
	$quesyvideo = new VideoRequest($pdo);
	
	$isadmins = $quesy -> isAdmins($userid);
	if($isadmins[0][0]!="1")
	{
		echo '<script language="JavaScript">;alert("Sorry you no have permission to operate!");location.href="/";</script>;';
		exit();
	}
	
	$arrayvideos = $quesy -> selectAll("videos");
	$totalvideos = count($arrayvideos);
	$fenleilist = $quesyvideo -> GetVideoList("1");
	$totfenlei = count($fenleilist);
	
?>

<html>
	<head>
		<title>
			视频后台管理
		</title>
		<link rel="icon" href="../img/sql.png" type="image/x-icon"/>
		<script src="./jquery-3.5.1.min.js"></script>
		<style>
			body,html{
				height: auto;
				margin: 0;
				padding: 0;
			    font-family:"Microsoft YaHei";
				background-color: #2F2F2F;
				color: #FF9900;
			}
			td{
				text-align:center;
				background-color: #252525;
			}
			th{
				background-color: #0f0f0f;
				color: white;
			}
			a{
				text-decoration: none;
				text-align: center;
				float:left;
				font-size: 15px;
				border:none;
				width: 64px;
				height: 20px;
				background: #ff9900;
				border-radius: 5px;
				color: black;
				cursor: pointer;
				transition: 0.3s;
		        margin:2px;
				}
				a:hover{
				    background: #ffa722;
				}
				input{
					width: 200px;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					padding: 12px 15px;
					font-size: 17px;
					margin: 7px auto;
					color: #ccc;
				}
				table{
					border-color: #555555;
					border-collapse: collapse;
					min-width: 1000px;
					top: 60px;
					position: relative;
				}
				table,table tr th, table tr td { border:1px solid #555555; }
				.headers{
					width: 100%;
					height: 50px;
					background-color: #1e1e1e;
					float: left;
					position: fixed;
					z-index: 99;
					top: 0;
					text-align: center;	
				}
				.searchcss{
					width: 9.375rem;
					height: 2.5rem;
					z-index: 102;
					position: fixed;
					float: left;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					font-size: 17px;
					color: #ccc;
					margin-left: 10px;
				}
				.searchcss2{
					width: 110px;
					height: 2.5rem;
					z-index: 102;
					position: fixed;
					float: left;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					font-size: 17px;
					color: #ccc;
					margin-left: 180px;
				}
				.searchcss3{
					width: 140px;
					height: 2.5rem;
					z-index: 102;
					position: fixed;
					float: left;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					font-size: 17px;
					color: #ccc;
					margin-left: 300px;
				}
				.selectcss{
					z-index: 102;
					position: fixed;
					float: left;
					margin-left: 460;
					height: 40px;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					font-size: 17px;
					margin-top: 7;
					color: #ccc;
				}
				.fenleibt{
					width: 60px;
					height: 38px;
					float: left;
					cursor: pointer;
					position: fixed;
					margin-left: 660;
					z-index: 102;
					top: 7;
					font-size: 15px;
					border:none;
					background: #ff9900;
					border-radius: 5px;
					color: black;
					transition: 0.3s;
				}
				.fenleibt:hover{
					background: #ffa722;
				}
			</style>
	</head>
	<body>
		  <div class="headers"></div>
		  <input  class="searchcss" placeholder="查找相关名称" name="key" type="text" id="key" onkeydown="onSearch(this)" value="" />
		  <input  class="searchcss2" placeholder="起始ID" name="key" type="text" id="selkey1" value="" />
		  <input  class="searchcss3" placeholder="终止ID-0则无" name="key" type="text" id="selkey2" value="0" />
		  <select id="sel" class="selectcss">
			  <option value =""></option>
		  	<?php
		  	for($as=1;$as<=$totfenlei;$as++){
		  		?>
		  		<option value ="<?php echo $fenleilist[$as];?>"><?php echo $fenleilist[$as];?></option>
		  		<?php
		  	}
		  	?>
		  </select>
		  <button class="fenleibt" onclick="getvalueo()" >更改</button>
		  
		  
		  <table  border="1" cellspacing="0" width="auto" align="center" id="store">
		  	<tr>
		
		  		<th>ID</th>
		  		<th>名称</th>
				<th>VID</th>
				<th>已被删除</th>
				<th>已被分类</th>
				<th>操作</th>
			</tr>
		  	
		  	<?php 
		  	   for($xs=0;$xs<$totalvideos;$xs++){
		  	 ?>
		  	<tr>
				<td><?php echo $arrayvideos[$xs][0];?></td>
				<td><?php echo $arrayvideos[$xs][1];?></td>
				<td><?php echo  $arrayvideos[$xs][2]; ?></td>
				<td><?php echo  $arrayvideos[$xs][3]; ?></td>
				<td><?php echo  $arrayvideos[$xs][4]; ?></td>
		  		<td style="width: 50;"><a onclick="DelFEN(<?php echo  $arrayvideos[$xs][0];?>)">删除</a></td>
		  	</tr>
		  		<?php
		  		   }
		  			?>
		  </table>
		  
		  <script type="text/javascript">
		  function onSearch(obj){//js函数开始
		    setTimeout(function(){//因为是即时查询，需要用setTimeout进行延迟，让值写入到input内，再读取
		      var storeId = document.getElementById('store');//获取table的id标识
		      var rowsLength = storeId.rows.length;//表格总共有多少行
		      var key = obj.value;//获取输入框的值
		      var searchCol = 1;//要搜索的哪一列，这里是第一列，从0开始数起
		      for(var i=1;i<rowsLength;i++){//按表的行数进行循环，本例第一行是标题，所以i=1，从第二行开始筛选（从0数起）
		        var searchText = storeId.rows[i].cells[searchCol].innerHTML;//取得table行，列的值
		        if(searchText.match(key)){//用match函数进行筛选，如果input的值，即变量 key的值为空，返回的是ture，
		          storeId.rows[i].style.display='';//显示行操作，
		        }else{
		          storeId.rows[i].style.display='none';//隐藏行操作
		        }
		      }
		    },200);//200为延时时间
		  }
		  </script>
		  <script>
		  function getvalueo(){
		  var selIds1 = document.getElementById('selkey1').value;
		  var selIds2 = document.getElementById('selkey2').value;
		  var sel=$("#sel option:checked").text();
		  $.get("/ALLBootP", { ot:"fenlei", op:selIds1,ay:sel,ou:selIds2});
		  }
		  function DelFEN(e){
			 $.get("/ALLBootP", { ot:"delsql", op:e}); 
		  }
		  </script>
	</body>
</html>
	

	
	
	
	