<?php
//后台管理用户数据库程序

header("content-type:text/html;charset=utf-8");
	session_start();
	$userid=$_SESSION['account'];
	$app = [];
	$app['config'] = require '../../config.php'; 
	require '../../Core/DataBase/ConnetDB.php';
	require '../../Core/DataBase/RequestDB.php';
	require '../../Request/VideoRequest.php';
	
	$pdo = ConnetDB::make($app['config']['database']);
	$quesy = new RequestDB($pdo);
	$quesyvideo = new VideoRequest($pdo);
	
	$isadmins = $quesy -> isAdmins($userid);
	if($isadmins[0][0]!="1")
	{
		echo '<script language="JavaScript">;alert("Sorry you no have permission to operate!");location.href="/";</script>;';
		exit();
	}
	
	$arr = $quesy -> selectAll("video");
	$totalarr = count($arr);
?>

<html>
	<head>
		<title>

			用户后台管理
			
		</title>
		<link rel="icon" href="/WebView/Static/Img/sql.png" type="image/x-icon"/>
		<script src="./jquery-3.5.1.min.js"></script>
		<style>
			body,html{
				background-color: #2F2F2F;
				color: #FF9900;
			}
			td{
				text-align:center;
				background-color: #252525;
			}
			th{
				background-color: #0f0f0f;
				color: white;
			}
			button{
				text-decoration: none;
				text-align: center;
				display: block;
				float:left;
				font-size: 15px;
				border:none;
				width: 64px;
				height: 40px;
				background: #ff9900;
				border-radius: 5px;
				color: black;
				cursor: pointer;
				transition: 0.3s;
		        margin:2px;
				}
				button:hover{
				    background: #ffa722;
				}
				input{
					width: 200px;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					padding: 12px 15px;
					font-size: 17px;
					margin: 7px auto;
					color: #ccc;
				}
				table{
					border-color: #555555;
					border-collapse: collapse;
					min-width: 1000px;
				}
				table,table tr th, table tr td { border:1px solid #555555; }
			</style>
	</head>
	<body>
		<div style="margin-left:200px;"><input  placeholder="查找用户名" name="key" type="text" id="key" onkeydown="onSearch(this)" value="" /></div>
		  <table border="1" cellspacing="0" width="auto" align="center" id="store" >
		  	<tr>
		
		  		<th>ID</th>
		  		<th>姓名</th>
		  		<th>用户名</th>
		  		<th>密码</th>
		  		<th>注册时间</th>
				<th>管理员</th>
				<th>上传权限</th>
				<th>已被删除</th>
				<th>操作</th>
		  	</tr>
		  	
		  	<?php 
		  	   for($a=0;$a<$totalarr;$a++){
		  	 ?>
		  	<tr>
				<td min-width="50"><?php echo $arr[$a][0];?></td>
				<td min-width="100"><?php echo $arr[$a][1];?></td>
		  		<td min-width="100"><?php echo $arr[$a][2];?></td>
		  		<td><?php echo $arr[$a][3];?></td>	
		  		<td><?php echo $arr[$a][4];?></td>
				<td><?php echo $arr[$a][5];?></td>
				<td><?php echo $arr[$a][6];?></td>
				<td><?php echo $arr[$a][7];?></td>
		  		<td min-width="350"><a onclick="deluser(<?php echo $arr[$a][0];?>)"><button>删除用户</button></a>
				<a onclick="addadmins(<?php echo $arr[$a][0];?>)"><button>添加管理员</button></a>
				<a onclick="deladmin(<?php echo $arr[$a][0];?>)"><button>取消管理员</button></a>
				<a onclick="addupload(<?php echo $arr[$a][0];?>)"><button>添加上传权限</button></a>
				<a onclick="delupload(<?php echo $arr[$a][0];?>)"><button>取消上传权限</button></a></td>
		  	</tr>
		  	<?php
			   }
		  		?>
		  </table>
	<script type="text/javascript">
	function onSearch(obj){//js函数开始
	  setTimeout(function(){//因为是即时查询，需要用setTimeout进行延迟，让值写入到input内，再读取
	    var storeId = document.getElementById('store');//获取table的id标识
	    var rowsLength = storeId.rows.length;//表格总共有多少行
	    var key = obj.value;//获取输入框的值
	    var searchCol = 2;//要搜索的哪一列，这里是第一列，从0开始数起
	    for(var i=1;i<rowsLength;i++){//按表的行数进行循环，本例第一行是标题，所以i=1，从第二行开始筛选（从0数起）
	      var searchText = storeId.rows[i].cells[searchCol].innerHTML;//取得table行，列的值
	      if(searchText.match(key)){//用match函数进行筛选，如果input的值，即变量 key的值为空，返回的是ture，
	        storeId.rows[i].style.display='';//显示行操作，
	      }else{
	        storeId.rows[i].style.display='none';//隐藏行操作
	      }
	    }
	  },200);//200为延时时间
	}
	function deluser(){
		 $.get("/ALLBootP", { ot:"deluser", op:e}); 
	}
	function addadmins(){
		 $.get("/ALLBootP", { ot:"addadmin", op:e}); 
	}
	function deladmin(){
		 $.get("/ALLBootP", { ot:"deladmin", op:e}); 
	}
	function addupload(){
		 $.get("/ALLBootP", { ot:"addupload", op:e}); 
	}
	function delupload(){
		 $.get("/ALLBootP", { ot:"delupload", op:e}); 
	}
	</script>
	</body>
</html>
	

	
	
	
	