<?php

//后台管理分类标签程序

header("content-type:text/html;charset=utf-8");
	session_start();
	$userid=$_SESSION['account'];
	$app = [];
	$app['config'] = require '../../config.php'; 
	require '../../Core/DataBase/ConnetDB.php';
	require '../../Core/DataBase/RequestDB.php';
	require '../../Request/VideoRequest.php';
	
	$pdo = ConnetDB::make($app['config']['database']);
	$quesy = new RequestDB($pdo);
	$quesyvideo = new VideoRequest($pdo);
	
	$isadmins = $quesy -> isAdmins($userid);
	if($isadmins[0][0]!="1")
	{
		echo '<script language="JavaScript">;alert("Sorry you no have permission to operate!");location.href="/";</script>;';
		exit();
	}
	
	$arrayfenlei = $quesyvideo -> GetVideoList("1");
	$totalfenlei = count($arrayfenlei);//计算数组的大小
	 
?>

<html>
	<head>
		<title>

			分类标签管理
			
		</title>
		<link rel="icon" href="../img/sql.png" type="image/x-icon"/>
		<script src="./jquery-3.5.1.min.js"></script>
		<style>
			body,html{
				background-color: #2F2F2F;
				color: #FF9900;
			}
			td{
				text-align:center;
				background-color: #252525;
			}
			th{
				background-color: #0f0f0f;
				color: white;
			}
			button{
				text-decoration: none;
				text-align: center;
				display: block;
				float:left;
				font-size: 15px;
				border:none;
				width: 100%;
				height: 40px;
				background: #ff9900;
				border-radius: 5px;
				color: black;
				cursor: pointer;
				transition: 0.3s;
		        margin:2px;
				}
				button:hover{
				    background: #ffa722;
				}
				input[type="text"]{
					width: 200px;
					box-sizing: border-box;
					border-radius: 2px;
					border: 1px solid #555;
					background: rgba(0,0,0,.9);
					padding: 12px 15px;
					font-size: 17px;
					margin: 7px auto;
					color: #ccc;
				}
				input[type="button"]{
				font-size: 15px;
				border:none;
				width: 64px;
				height: 40px;
				background: #ff9900;
				border-radius: 5px;
				color: black;
				cursor: pointer;
				transition: 0.3s;
		        margin:2px;
				}
				input[type="button"]:hover{
					background: #ffa722;
				}
				table{
					border-color: #555555;
					border-collapse: collapse;
					min-width: 1000px;
				}
				table,table tr th, table tr td { border:1px solid #555555; }
			</style>
	</head>
	<body>
		<div style="margin-left: 200px;">
		<input type="text" placeholder="添加标签" name="" value="" id="na">
		<input type="button" value="添加" id="but">
		</div>
		  <table border="1" cellspacing="0"  width="1000" align="center">
		  	<tr>
		
		  		<th>分类标签</th>
				<th>操作</th>
		  	</tr>
			
			<?php
			for($o=1;$o<=$totalfenlei;$o++){
			?>
			<tr>
				<td><?php echo $arrayfenlei[$o];?></td>
				<td><a onclick="Geat(<?php echo $o;?>)"><button>删除该分类标签</button></a></td>
			</tr>
			<?php	
			}
			?>
		  </table>
		  <script>
		  $(function(){
		  $("#but").click(function(){
		  var na =$("#na").val();
		  var post = na;
		  if(na==null||na==""){
			  alert("请填入有效的分类名称!");
			  return
		  }else{
			  $.get("/ALLBootP", { ot:"setfenlei", op:post});
			  window.location.reload();
		  }
		  })
		  })
		  function Geat(a){
			 $.get("/ALLBootP", { ot:"unsetfenlei", op:a});
			 window.location.reload();
		  }
		  </script>
	</body>
</html>
	

	
	
	
	