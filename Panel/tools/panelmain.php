<?php

	header("content-type:text/html;charset=utf-8");
	session_start();
	$userid=$_SESSION['account'];
	$app = [];
	$app['config'] = require '../../config.php'; 
	require '../../Core/DataBase/ConnetDB.php';
	require '../../Core/DataBase/RequestDB.php';
	require '../../Request/VideoRequest.php';
	
	$pdo = ConnetDB::make($app['config']['database']);
	$quesy = new RequestDB($pdo);
	$quesyvideo = new VideoRequest($pdo);
	
	$isadmins = $quesy -> isAdmins($userid);
	if($isadmins[0][0]!="1")
	{
		echo '<script language="JavaScript">;alert("Sorry you no have permission to operate!");location.href="/";</script>;';
		exit();
	}
	
	$videosn = $quesy -> selectAll("videos");
	$videon = $quesy -> selectAll("video");
	$intvideo = count($videosn);
	$intuser = count($videon);
	
	$fenleilist = $quesyvideo -> GetVideoList("1");
	$totalfe = count($fenleilist);
	
	//var_dump($videosn[0][4]);
	$arrayint []="";
	for($x=1;$x<($totalfe+1);$x++){
		$aseb = 0;
		for($a=0;$a<$intvideo;$a++){
			
			if($videosn[$a][4]==$fenleilist[$x]){
				$aseb++;
			}
			
		}
		array_push($arrayint,$aseb);
	}
	$totalint = count($arrayint);
	
	
	function get_disk_total(int $total) : string
	{
	    $config = [
	        '3' => '',
	        '2' => '',
	        '1' => ''
	    ];
	    foreach($config as $key => $value){
	        if($total > pow(1024, $key)){
	            return round($total / pow(1024,$key)).$value;
	        }
	        return $total;
	    }
	}
	$disre = get_disk_total(disk_total_space('.')) - get_disk_total(disk_free_space('.'));
	$disto = get_disk_total(disk_total_space('.'));
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="./echarts.min.js"></script>
	<style>
	body,html{
	    background-color: #000000;
		color: #FF9900;
	}
	.out{
		border:1px solid #1F1F1F;
		margin: 20px 20px;
		box-shadow:0px 0px 5px 3px #2F2F2F;
		width:600px;
		height:600px;
		float:left;
	}
	</style>
</head>
<body>
    <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    <div class="out" id="main" ></div>
	<div class="out" id="divers"></div>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        // 指定图表的配置项和数据
        var option = {
    tooltip: {
        trigger: 'item'
    },
    legend: {
        top: '5%',
        left: 'center',
		textStyle:{
		    color:'#FF9900'
		}
    },
    series: [
        {
            name: '数据库总览',
            type: 'pie',
            radius: ['40%', '70%'],
            avoidLabelOverlap: false,
            itemStyle: {
                borderRadius: 5,
                borderWidth: 2
            },
            label: {
                show: false,
                position: 'center',
				color: '#FF9900'
            },
            emphasis: {
                label: {
                    show: true,
                    fontSize: '20',
                    fontWeight: 'bold',
                }
            },
            labelLine: {
                show: false,
				color: '#FF9900'
            },
            data: [
                {value:<?php echo $intvideo; ?>, name: '总计Video' },
				<?php
				for($a=1;$a<=($totalint-1);$a++){
				echo'{value:'.$arrayint[$a].', name: "分类'.$fenleilist[$a].'" },';	
				}
				?>
                {value:<?php echo $intuser; ?>, name: '用户总数' }
            ]
			
        }
    ]
};
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
    </script>
	   <script type="text/javascript">
	        // 基于准备好的dom，初始化echarts实例
	        var myChart = echarts.init(document.getElementById('divers'));
	        // 指定图表的配置项和数据
	        var option = {
	    series: [{
	            type: 'gauge',
	            center: ["50%", "60%"],
	            startAngle: 200,
	            endAngle: -20,
	            min: 0,
	            max: <?php echo $disto; ?>,
	            splitNumber: 10,
	            itemStyle: {
					color: {
					        type: 'linear', // 线性渐变
					        x: 0,
					        y: 0,
					        x2: 0,
					        y2: 1,
					        colorStops: [{
					            offset: 0,
					            color: '#d4fc79' // 0%处的颜色为红色
					        }, {
					            offset: 1,
					            color: '#96e6a1' // 100%处的颜色为蓝
					            }]
					        }
	            },
	            progress: {
	                show: true,
	                width: 30
	            },
	
	            pointer: {
	                show: false,
	            },
	            axisLine: {
	                lineStyle: {
	                    width: 30
	                },
					
	            },
	            axisTick: {
	                distance: -45,
	                splitNumber: 5,
	                lineStyle: {
	                    width: 2,
	                    color: '#999'
	                }
	            },
	            splitLine: {
	                distance: -52,
	                length: 14,
	                lineStyle: {
	                    width: 3,
	                    color: '#999'
	                }
	            },
	            axisLabel: {
	                distance: -20,
	                color: '#999',
	                fontSize: 20,
					formatter: function(v){
					    return v.toFixed(0);
					}
	            },
	            anchor: {
	                show: false
	            },
	            title: {
	                show: false
	            },
	            detail: {
	                valueAnimation: true,
	                width: '60%',
	                lineHeight: 40,
	                height: '15%',
	                borderRadius: 8,
	                offsetCenter: [0, '-15%'],
	                fontSize: 20,
	                fontWeight: 'bolder',
	                formatter: '已存储容量{value}G',
	                color: '#7F7F7F',
	            },
	            data: [{
	                value: <?php echo $disre;?>
	            }]
	        },
	
	        {
	            type: 'gauge',
	            center: ["50%", "60%"],
	            startAngle: 200,
	            endAngle: -20,
	            min: 0,
	            max: <?php echo $disto; ?>,
	            itemStyle: {
					color: {
					        type: 'linear', // 线性渐变
					        x: 0,
					        y: 0,
					        x2: 0,
					        y2: 1,
					        colorStops: [{
					            offset: 0,
					            color: '#84fab0' // 0%处的颜色为红色
					        }, {
					            offset: 1,
					            color: '#8fd3f4' // 100%处的颜色为蓝
					            }]
					        }
	            },
	            progress: {
	                show: true,
	                width: 8
	            },
	
	            pointer: {
	                show: false
	            },
	            axisLine: {
	                show: false
	            },
	            axisTick: {
	                show: false
	            },
	            splitLine: {
	                show: false
	            },
	            axisLabel: {
	                show: false
	            },
	            detail: {
	                show: false,
	            },
	            data: [{
	                value: <?php echo $disre;?>,
	            }]
	
	        }
	    ],
	};
	        // 使用刚指定的配置项和数据显示图表。
	        myChart.setOption(option);
	    </script>
</body>
</html>
