<?php
session_start();
if(isset($_SESSION["account"])){
	$user = $_SESSION["account"];
}else{
	Header('location:/login?admin=1');
	exit();
}
$app = [];
$app['config'] = require 'config.php'; 
require 'Core/DataBase/ConnetDB.php';
require 'Core/DataBase/RequestDB.php';
$pdo = ConnetDB::make($app['config']['database']);
$quesy = new RequestDB($pdo);
$isRegister = $quesy -> isRegister();
$isAdmins = $quesy -> isAdmins($user);

if($isAdmins[0][0]!="1"){
	Header('location:/login?admin=1');
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="/WebView/Static/Img/panel.png" type="image/x-icon"/>
    <title>Video Panel</title>
	<style>
	body,html{
	    background: black;
		font-family:"Microsoft YaHei";
		margin: 0px;
		padding: 0px;
		width: 100%;
		height: 100%;
	}
	h1{
	    color: aliceblue;
	    text-align: center;
		font-family:"Microsoft YaHei";
	}
		#btn{
			float:left;
		    font-size: 15px;
		    border:none;
		    width: 100%;
		    background: #ff9900;
		    border-radius: 5px;
		    height: 50px;
		    color: white;
		    cursor: pointer;
		    transition: 0.3s;
			margin:2px;
		}
		#btn:hover{
		    background: #ffa722;
		}
		ul{list-style: none;}
	</style>
</head>
<body>
<iframe frameborder="0" id="myframe" style="float:right;background-color: white;" width="85%" height="100%" src="./Panel/tools/panelmain.php" name="right"></iframe>
<div style=float:left;text-align:center;width:10%;height:100%;>
<h1>Video Panel</h1>
<ul>
<li><a href="/Panel/tools/panelmain.php" target="right"><button type="button" id="btn" >总览后台</button></a></li>
<li><a href="/Panel/tools/usersql.php" target="right"><button type="button" id="btn" >用户后台管理</button></a></li>
<li><a href="/Panel/tools/sqlvideo.php" target="right"><button type="button" id="btn" >视频后台管理</button></a></li>
<li><a href="/Panel/tools/showfenlei.php" target="right"><button type="button" id="btn" >分类标签管理</button></a></li>
<li><a onclick="inputaddvideo()"><button type="button" id="btn" >添加Temp视频导入</button></a></li>
<li><a onclick="inputvioce()"><button type="button" id="btn" >添加Temp声音导入</button></a></li>
<li><a href="/Panel/tools/uploadvideo.php"  target="right"><button type="button" id="btn" >上传视频</button></a></li>
<li><a onclick="oepn()"><button type="button" id="btn" >前往视频首页</button></a></li>
<?php
if($isRegister[0][0]!="1"){
	echo '<li><a href="operation/postisreg.php?sure=1"><button type="button" id="btn" >开启注册功能</button></a></li>';
}else{
	echo '<li><a href="operation/postisreg.php?sure=0"><button type="button" id="btn" >关闭注册功能</button></a></li>';
}
?>
<li><a href="/Panel/tools/info.php" target="right"><button type="button" id="btn" >查看当前PHP</button></a></li>
</ul>
</div>


<script>
function inputaddvideo(){
	var x;
	var r=confirm("是否确认手动导入video?");
	if (r==true){
		document.getElementById("myframe").src="/addvideo?sure=1";
	}
	else{
		
	}
}
function inputvioce(){
	var x;
	var r=confirm("是否确认手动导入video?");
	if (r==true){
		document.getElementById("myframe").src="/addvoice?sure=1";
	}
	else{
		
	}
}
function oepn(){
	window.open("/videos?go=all"); 
}
</script>
</body>
</html>