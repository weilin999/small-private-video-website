<?php
     $id = $_GET["vo"];
 ?>
<html>
	<head>
		<meta charset="utf-8">
		<title>Video Home</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
		<link rel="icon" href="./WebView/Static/Img/pco.png" type="image/x-icon"/>
		<script src="./WebView/Static/Js/DPlayer.min.js"></script>
		<style>
		body{color: #FFFFFF;
		text-align: center;}
		#btn{
		    font-size: 15px;
		    border:none;
		    width: 160px;
		    background: #ff9900;
		    border-radius: 5px;
		    height: 30px;
		    color: aliceblue;
		    cursor: pointer;
		    transition: 0.3s;
			margin:0 0 5px;
			float: left;
			position: fixed;
			left: 0;
			
		}
		#btn:hover{
		    background: #d4850f;
		}
		.box{width:400px;}
		#showtime{margin:20px;margin-bottom: 20px;text-align: center;}
		</style>
	</head>
	<body bgcolor="black";>
		<center>
		<div>
			<span id="showtime1">00</span>
			<span>:</span>
			<span id="showtime2">00</span>
			<span>:</span>
			<span id="showtime3">00</span>
		</div>
		<div style="margin-bottom: 10px;"><a style=color:white;></a></div>
		<div style="width: 100%;height: 90%;" id="dplayer"></div>
		</center>

		
		<script>
		//添加事件
				var min=0;
				var sec=0;
				var ms=0;
				var timer=null;
				var count=0;
				var sport=1;
				var pai=0;
		//生成时间
			function show(){
				ms++;
				if(sec==60){
					min++;sec=0;
				}
				if(ms==100){
					sec++;ms=0;
				}
				var msStr=ms;
				if(ms<10){
					msStr="0"+ms;
				}
				var secStr=sec;
				if(sec<10){
					secStr="0"+sec;
				}
				var minStr=min;
				if(min<10){
					minStr="0"+min;
				}
				document.getElementById("showtime1").innerHTML = minStr;
				document.getElementById("showtime2").innerHTML = secStr;
				document.getElementById("showtime3").innerHTML = msStr;
				//$('#showtime span:eq(0)').html(minStr);
				//$('#showtime span:eq(2)').html(secStr);
				//$('#showtime span:eq(4)').html(msStr);
			}
			
		const dp = new DPlayer({
		    container: document.getElementById('dplayer'),
		    autoplay: false,
			volume: 1,
		    video: {
		        url: '../video/<?php echo $id;?>.mp4',
		    },
		 /*   contextmenu: [
		        {
		            text: 'custom1',
		            link: '',
		        },
		    ],*/
		    highlight: [
		        {
		            time: 120,
		            text: '这是第 2 分钟',
		        },
		        {
		            time: 300,
		            text: '这是 5 分钟',
		        },
				{
		            time: 600,
		            text: '这是 10 分钟',
		        },
				{
		            time: 1800,
		            text: '这是 30 分钟',
		        },
				{
		            time: 3600,
		            text: '这是 1 小时',
		        },
		    ],
		});
		// 进行监听
		dp.on('play', function () {
			//console.log("播放");
			clearInterval(timer);
			timer=setInterval(show,10);
		});
		dp.on('pause', function () {
			//console.log("暂停");
			clearInterval(timer);
		});
		</script>
	</body>
</html>