<?php

$pdo = ConnetDB::make($app['config']['database']);
$quesy = new RequestDB($pdo);
$isRegister = $quesy -> isRegister();
$gogo = null;
if(isset($_GET['admin'])){
	$gogo = $_GET['admin'];
}

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="./WebView/Static/Img/logo.png" type="image/x-icon"/>
	<link rel="stylesheet" href="./WebView/Static/Css/login.css"/>
    <title>Video Login</title>
</head>
<body>
    <div class="logo-box">
        <div class="login" id="isme">
            <div>
                <h1>Login Video</h1>
            </div>
				<?php if($gogo =="1"){ 
				echo '<form action="/login?admin=1" autocomplete="off" method="post">';
				 }else{
				echo '<form action="/login" autocomplete="off" method="post">';
				}?>
				<div class="bxs-row">
					<input type="text" autocomplete="off" id="account" class="input" name="account" placeholder="Account">
				</div>
				<div class="bxs-row">
					<input type="password" autocomplete="off" id="password" class="input" name="password" placeholder="Password">
				</div>
				<div class="bxs-row">
					<?php 	
					if($isRegister[0][0] == "1")
					{
						echo '<input type="submit" class="submit" value="Login">';
						echo '<a href="register.php"><button type="button" class="reg" >Register</button></a>';
					}else{
						echo '<input type="submit" class="submit btn" value="Login">';
					}
					?>
				</div>
			</form>
        </div>
    </div>
</body>
</html>