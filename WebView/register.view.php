<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="icon" href="./WebView/Static/Img/logo.png" type="image/x-icon"/>
    <title>Video Register</title>
	<link rel="stylesheet" href="./WebView/Static/Css/register.css">
</head>
<body>
    <div class="logo-box">
        <div class="login" id="isme">
            <div class="bxs-row" style="text-align:center;">
                <h1>Register Video</h1>
            </div>
			<form action="/register" autocomplete="off" method="post">
			<div class="bxs-row">
			 <input value="" onKeyUp="this.value=this.value.replace(/\s+/g,'')" onKeypress="if ((event.keyCode > 32 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97)) event.returnValue = false;" autocomplete="off" class="username"  placeholder="Name" type="text" name="nickName">
			</div>
			<div class="bxs-row">
			  <input value="" onKeyUp="value=value.replace(/[\W]/g,'')" onKeypress="if ((event.keyCode > 32 && event.keyCode < 48) || (event.keyCode > 57 && event.keyCode < 65) || (event.keyCode > 90 && event.keyCode < 97)) event.returnValue = false;" autocomplete="off" class="username" placeholder="Account" type="text" name="userName">
			</div>
			<div class="bxs-row">
			  <input value="" onKeyUp="value=value.replace(/\s+/g,'')" autocomplete="off" class="password" placeholder="Password" type="password" name="pwd">
			</div>
            <div class="bxs-row">
				<input type="submit" class="submit" value="Register">
				<a href="index.php"><button type="button" class="reg" ><font color="#000000"><b>Return Login</b></font></button></a>
            </div>
			</form>
        </div>
    </div>
</body>
</html>