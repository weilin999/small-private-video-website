
<html>
	<head>
		<meta charset="utf-8">
		<title>VideosHub</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
		<link rel="icon" href="./WebView/Static/Img/vh.png" type="image/x-icon"/>
		<link rel="stylesheet" href="./WebView/Static/Css/video.css">
		<script src="./WebView/Static/Js/vue3.js"></script>
		<script src="./WebView/Static/Js/axios.js"></script>
		<script src="./WebView/Static/iconfont/iconfont.js"></script>
	</head>
	<body>
	<div id="app">
		<!-- 左侧导航 -->
		<div id="left_dh">
		<h3>用户</h3>
		<div class="accountdiv">
			<img src="./WebView/Static/Img/account.png" />
			<div class="accountname">
				<?php session_start();
				  echo $_SESSION['name']; ?>
			</div>
		</div>
		<h3>操作</h3>
		<div class="menudi">
		<a  class="a1" @click="LoginOut" ><svg class="icon" aria-hidden="true"><use xlink:href="#icon-dengchu"></use></svg><span><br>登出</span></a>
		<?php
		if($_SESSION['isloader']=="1"){ 
		echo "<a class=a3 href=/uploadvideo  target=_blank><svg class=icon aria-hidden=true><use xlink:href=#icon-shangchuan></use></svg><span><br>上传</span></a>";
		}
		 if($_SESSION['isadmin']=="1")
		{
			echo "<a class=a4 href=/panels  target=_blank><svg class=icon aria-hidden=true><use xlink:href=#icon-houtai></use></svg><span><br>后台</span></a>";
		} ?>
		</div>
		<div class="leftdi">
		<h3>视频分类</h3>
		<a href="?go=all">全部视频</a>
		<a href="?go=lo">个人喜欢</a>
		<a href="?go=按观看排序">按观看排序</a>
		<div v-for="list in listdata">
			<a :href="'?go='+list">{{list}}</a>
		</div>
		</div>
		</div>
		
		<a class="dh_btn" id="menubt" onclick="openLeft_dh()"><img id="imgmenu" src="./WebView/Static/Img/menu.png"></a>
		<div id="black_bg" onclick="closeLeft_dh()"></div>
		<div id="black_bg2" onclick="close_Seach()"></div>
		<a class="findbut" onclick="searchbut()" ><svg class="icon" aria-hidden="true"><use xlink:href="#icon-weibiaoti108"></use></svg></a>
		<input type="text" autocomplete="off" placeholder="查找相关视频" id="search" class="searchcss" >		
		<div>
			<a onclick="returnTop()" id="btnTop" title="返回顶部"><img width="50" height="50"  src="./WebView/Static/Img/top.png"></a>
		</div>
		
		<div class="headers">
		<a style=line-height:1.7;font-size:30;color:white;width:120px;>Videos</a>&nbsp<a style=font-size:30;width:150px;background:#ff9900;border-radius:5px;height:50px;color:black;>&nbspHub&nbsp</a>
		</div>
		<div class="zhanwei" >
		</div>
		
		<!--列表-->
		
		<div class="video-list-main" >
			<div v-for="video in videodata" :key="video.id">
				<a v-if="video.isdel==false" id="card" :class="'video-card-v'+[calsss]" >
					<div :class="'video-card'+[calsss]">
						<img @click="Comevideo(video.vid)" :src="'../videoimg/'+video.vid+'.png'"  :class="'pic'+[calsss]"/>
						<div :class="'video-count'+[calsss]">
							观看次数：{{video.watch}}
						<div v-if="calsss == 1"><button v-if="video.islove == false" :id="'buloves'+[calsss]" @click="ADDLove(video.id);video.islove=true" style=width:30px;height=20px;>
						<img class="addlovescss" src="./WebView/Static/Img/loves.png"/></button>
						<button v-if="video.islove == true" :id="'buloves'+[calsss]" @click="DELLove(video.id);video.islove=false" style=width:30px;height=20px;>
						<img class="addlovescss" src="./WebView/Static/Img/isloves.png"/></button></div>
						<div v-if="calsss == ''"><button v-if="video.islove == false" :id="'buloves'+[calsss]" @click="ADDLove(video.id);video.islove=true" style=width:100px;height=20px;>
						<img class="addlovescss" src="./WebView/Static/Img/loves.png"/>&nbsp加入喜欢</button>
						<button v-if="video.islove == true" :id="'buloves'+[calsss]" @click="DELLove(video.id);video.islove=false" style=width:100px;height=20px;>
						<img class="addlovescss" src="./WebView/Static/Img/isloves.png"/>&nbsp取消喜欢</button></div>
						</div>
					</div>
					<p class="video-card-title">{{video.name}}</p>
				</a>
			</div>
		</div>	
		
	</div>
	<script src="./WebView/Static/Js/video.js"></script>
	<script src="./WebView/Static/Js/videoScrip.js"></script>
</body>
</html>
