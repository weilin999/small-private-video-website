var mainDiv = Vue.createApp({
  data() {
	return {
		videodata: [],
		where: '',
		search: '',
		listdata:[],
		calsss: ''
	}
  },
  created() {
		this.ISMOB();
		this.GetList();
		this.where = this.GetPHP("go");
		if(this.where != null){
			this.GoToVideo(this.where);
		}
		this.search = this.GetPHP("search");
		if(this.search != null){
			this.SearchTo(this.search);
		}
	},
  methods: {
		GoToVideo (where) {
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "GetVideo",
					class: where
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				this.videodata = res.data;
			})
	    },
	    LoginOut(){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "loginout",
				},
				transformRequest: [
					function (data) {
						let ret = ''
						for (let it in data) {
							ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
						}
						ret = ret.substring(0, ret.lastIndexOf('&'));
						return ret
					}
				],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				if(res.data=="1"){
					location.href='';
				}
			})
		},
		GetPHP(name){
			return decodeURIComponent(
			(new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.href) || [, ""])[1].replace(/\+/g, '%20')) || null;
		},
		SearchTo(sen){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "tosearch",
					class: sen
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				this.videodata = res.data;
			})
		},
		GetList(){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "getlist"
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				this.listdata = res.data;
				//console.log(this.list);
			})
		},
		ADDLove(id){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "addlove",
					id:id
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				console.log(res.data);
				//this.listdata = res.data;
				//console.log(this.list);
			})
		},
		DELLove(id){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "dellove",
					id:id
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			}).then((res)=>{
				console.log(res.data);
				//console.log(this.list);
			})
		},
		IWatch(){
			axios({
					method: 'post',
					url: '/requestvideo',
					data: {
					connet: "dellove",
					id:id
				},
				transformRequest: [
				function (data) {
					let ret = ''
					for (let it in data) {
						ret += encodeURIComponent(it) + '=' + encodeURIComponent(data[it]) + '&'
					}
					ret = ret.substring(0, ret.lastIndexOf('&'));
					return ret
				}],
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				}
			})
		},
		ISMOB(){
			if(this._isMobile()){
				console.log("1");
				this.calsss = "1"
			}else{
				console.log("0");
				this.calsss = ""
			}
		},
		_isMobile() {
		      let flag = navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i);
		      return flag;
		},
		Comevideo(vid){
			window.open('./comevideo?vo='+vid, '_blank');
			//location.href='./comevideo/?vo='+vid;
		}
    }
});
mainDiv.mount("#app");