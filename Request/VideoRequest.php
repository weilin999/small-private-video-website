<?php

	class video {
		public $id;
		public $name;
		public $vid;
		public $isdel;
		public $islove = false;
		public $loveid = null;
		public $watch;
	}

class VideoRequest {
	
	protected $pdo;
	
	public function __construct($pdo){
		
		$this->pdo = $pdo;
		
	}
	
	
	public function GetVideo ($where,$userid){
		$statement = $this -> pdo->prepare("select * from videos");

		$statement -> execute();
		$Data =  $statement -> fetchAll();
		$datas = null;
		$TotalData = count($Data);
		$Lovesvideo = $this -> UserLovers($userid);
		$TOtalLove = count($Lovesvideo);
		
		if($where == "all" or $where == "全部分类"){
			for($s=0;$s<$TotalData;$s++){
				$videos = new video();
				$videos -> id = $Data[$s][0];
				$videos -> name = $Data[$s][1];
				$videos -> vid = $Data[$s][2];
				$videos -> isdel = $Data[$s][3];
				$videos -> watch = $Data[$s][5];
				if($TOtalLove != null){
					for($j=1; $j<$TOtalLove;$j++){
						if($Data[$s][0] == $Lovesvideo[$j])
						{
							$videos -> islove = true;
							$videos -> loveid = $s;
						}
					}
				}
				$datas[]=$videos;
			}
			return $datas;
		}elseif($where == "lo" or $where == "我喜欢的"){
			if($TOtalLove != 0){
				for($i=1;$i<$TOtalLove;$i++){
					$videolo = new video();
					$videolo -> id = $Data[$Lovesvideo[$i]][0];
					$videolo -> name = $Data[$Lovesvideo[$i]][1];
					$videolo -> vid = $Data[$Lovesvideo[$i]][2];
					$videolo -> isdel = $Data[$Lovesvideo[$i]][3];
					$videolo -> watch = $Data[$Lovesvideo[$i]][5];
					$videolo -> islove = true;
					$videolo -> loveid = $i;
					$datas[]=$videolo;
				}
			}else{
				$datas[]="";
			}
			return $datas;
		}elseif($where=="按观看排序"){
			$statement = $this -> pdo->prepare("SELECT * FROM videos ORDER BY watch DESC");
	
			$statement -> execute();
			$dataFE =  $statement -> fetchAll();
			for($s=0;$s<$TotalData;$s++){
				$videos = new video();
				$videos -> id = $dataFE[$s][0];
				$videos -> name = $dataFE[$s][1];
				$videos -> vid = $dataFE[$s][2];
				$videos -> isdel = $dataFE[$s][3];
				$videos -> watch = $dataFE[$s][5];
				if($TOtalLove != null){
					for($j=1; $j<$TOtalLove;$j++){
						if($dataFE[$s][0] == $Lovesvideo[$j])
						{
							$videos -> islove = true;
							$videos -> loveid = $s;
						}
					}
				}
				$datas[]=$videos;
			}
			return $datas;
		}else{
			
			$Fenlei = $this -> GetVideoList(2);
			$Totallei = count($Fenlei);
			for($a=1;$a<$Totallei+1;$a++){
				if($where == $Fenlei[$a]){
					for($s=0;$s<$TotalData;$s++){
						if($Data[$s][4] == $where){
							$videos = new video();
							$videos -> id = $Data[$s][0];
							$videos -> name = $Data[$s][1];
							$videos -> vid = $Data[$s][2];
							$videos -> isdel = $Data[$s][3];
							$videos -> watch = $Data[$s][5];
							if($TOtalLove != null){
								for($j=1; $j<$TOtalLove;$j++){
									if($Data[$s][0] == $Lovesvideo[$j])
									{
										$videos -> islove = true;
										$videos -> loveid = $s;
									}
								}
							}
							$datas[]=$videos;
						}
					}
				}
			}
			return $datas;
		}
		
	}
	
	public function UserLovers ($userid){
		
		$statement = $this -> pdo->prepare("select loves from loves where id='$userid'");

		$statement -> execute();
		$strloves = $statement -> fetchAll();
		if($strloves == null){
			$arrayloves = null;
		}else{
			$arrayloves = explode("/*/*/",$strloves[0][0]);
		}
		return $arrayloves;
		
	}
	
	
	public function ToSearch ($nam,$user){
		
		$LovesVi = $this -> UserLovers($user);
		if($LovesVi == null)
		{
			$TotalVi = null;
		}else{
			$TotalVi = count($LovesVi);
		}
		
		$statement = $this -> pdo->prepare("select * from videos");

		$statement -> execute();
		$ALlVideo =  $statement -> fetchAll();
		$TotalVideo = count($ALlVideo);
		
		for($i=0; $i<$TotalVideo; $i++){
			
			if (strstr($ALlVideo[$i][1],$nam) !== false ){
				
				if($ALlVideo[$i][3]=="0"){
					
					$videoae = new video();
					$videoae -> id = $ALlVideo[$i][0];
					$videoae -> name = $ALlVideo[$i][1];
					$videoae -> vid = $ALlVideo[$i][2];
					$videoae -> isdel = $ALlVideo[$i][3];
					$videoae -> watch = $ALlVideo[$i][5];
					
					if($TotalVi != null){
						for($j=1; $j<=$TotalVi;$j++){
							if($ALlVideo[$i][0] == $LovesVi[$j])
							{
								$videoae -> islove = true;
								$videoae -> loveid = $j;
							}
						}
					}
				
				}
				$data[]=$videoae;
			}
			
		}
		return json_encode($data, JSON_UNESCAPED_UNICODE);
		//return count($ALlVideo);
		
	}
	
	public function GetVideoList($e=0){
		
		$statement = $this -> pdo->prepare("select fenlei from panel where id=1");

		$statement -> execute();
		$List = $statement -> fetchAll();
		$arrayfenlei = explode("/*/*/",$List[0][0]);
		unset($arrayfenlei[0]);
		if($e==0){
			$arrayfenlei2 = array('全部分类', '我喜欢的','按观看排序');
			array_splice($arrayfenlei, 0, 0, $arrayfenlei2); // 插入到位置3且删除0个
			return json_encode($arrayfenlei, JSON_UNESCAPED_UNICODE);
		}else{
			return $arrayfenlei;
		}
		//return $arrayfenlei;
		
	}
	
	public function AddLove($id,$userid){
		
		$statement = $this -> pdo->prepare("select loves from loves where id='$userid'");

		$statement -> execute();
		$strloves = $statement -> fetchAll();
		$adddata = $strloves[0][0]."/*/*/".$id;
		$statement = $this -> pdo->prepare("UPDATE loves SET loves='$adddata' WHERE id='$userid'");
		$statement -> execute();
		if($statement -> rowCount() > 0){
			$arrpo = array('status'=>'success');
			echo json_encode($arrpo);
		}else{
			$arrpo = array('status'=>'failed');
			echo json_encode($arrpo);
		}
		
	}
	
	public function DELLove($id,$userid){
		
		$statement = $this -> pdo->prepare("select loves from loves where id='$userid'");

		$statement -> execute();
		$strloves = $statement -> fetchAll();
		$deldata = explode("/*/*/",$strloves[0][0]);//分割数据
		unset($deldata[$id]);
		$deldata = array_values($deldata);
		$totoaldel = count($deldata);
		$adddata = "";
		for($o=1;$o<=($totoaldel-1);$o++)
		{
				$adddata = $adddata."/*/*/".$deldata[$o];
		}
		$statement = $this -> pdo->prepare("UPDATE loves SET loves='$adddata' WHERE id='$userid'");
		$statement -> execute();
		if($statement -> rowCount() > 0){
			$arrpo = array('status'=>'success');
			echo json_encode($arrpo);
		}else{
			$arrpo = array('status'=>'failed');
			echo json_encode($arrpo);
		}
		
	}
	
	public function Login($acc,$pass){
		$islogin = $this -> pdo->prepare("select * from video where userName='$acc' and pwd='$pass'");
		$islogin -> execute();
		$sql =  $islogin -> fetchAll();
		if($sql == null){
			$arrpos = array('status'=>'2');
			return json_encode($arrpos);
		}elseif($sql[0] != null){
			session_start();
			$_SESSION["account"] = $sql[0]['userName'];
			$_SESSION["accountid"] = $sql[0]['id'];
			$_SESSION["name"] = $sql[0]['nickName'];
			$_SESSION["isloader"] = $sql[0]['isupload'];
			$_SESSION["isadmin"] = $sql[0]['isadmin'];
			$arrpo = array('status'=>'success','id'=>$sql[0]['id']);
			return json_encode($arrpo);
		}
	}
	
	public function IWatch($id){
		$statement = $this -> pdo->prepare("select watch from videos where id='$id'");
		$statement -> execute();
		$watchis =  $statement -> fetchAll();
		$ads = $watchis[0][0];
		$ads = $ads+1;
		//return $ads;
		$statement = $this -> pdo->prepare("UPDATE videos SET watch='$ads' WHERE id='$id'");
		$statement -> execute();
		if($statement -> rowCount() > 0){
			$arrpo = array('status'=>'success');
			echo json_encode($arrpo);
		}else{
			$arrpo = array('status'=>'failed');
			echo json_encode($arrpo);
		}
		
	}	
	
	public function Test(){
		$arrpo = array('status'=>'failed');
		echo json_encode($arrpo);
	}
	
	
}

?>