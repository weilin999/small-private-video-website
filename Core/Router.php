<?php

class Router{
	
	protected $routers = [];
	
	public static function load($file){
		
		$router = new Router;
		require $file;
		return $router;
		
	}
	
	public function define($routers){
		
		$this -> routers = $routers;
		
	}
	
	public function direct($url){
		if(array_key_exists($url,$this->routers)){
			return $this->routers[$url];
		}else{
			require 'Message/error.php';
			exit('未找到指定的页面');
		}
	}
	
}

?>