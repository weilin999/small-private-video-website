/*
 Navicat MySQL Data Transfer

 Source Server         : 本地Mysql
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : video

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 30/03/2022 21:12:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for loves
-- ----------------------------
DROP TABLE IF EXISTS `loves`;
CREATE TABLE `loves`  (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '顺序ID',
  `loves` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '个人喜欢分割集',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户对应的loves数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of loves
-- ----------------------------

-- ----------------------------
-- Table structure for panel
-- ----------------------------
DROP TABLE IF EXISTS `panel`;
CREATE TABLE `panel`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '顺序ID',
  `isregister` int(11) NOT NULL DEFAULT 0 COMMENT '注册权限1为开启0为不开启',
  `fenlei` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '分类标签',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '后台数据库' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of panel
-- ----------------------------
INSERT INTO `panel` VALUES (1, 0, '');

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video`  (
  `id` int(255) NOT NULL AUTO_INCREMENT COMMENT '顺序ID',
  `userName` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '账号',
  `nickName` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '名称',
  `pwd` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '密码',
  `regTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `isadmin` int(11) NOT NULL DEFAULT 0 COMMENT '是否为管理员',
  `isupload` int(11) NOT NULL DEFAULT 0 COMMENT '是否为上传者',
  `isdel` int(11) NOT NULL DEFAULT 0 COMMENT '是否已被删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '用户数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of video
-- ----------------------------
INSERT INTO `video` VALUES (1, 'wu', '测试', 'd3cb757121f725fe825a1176031a1c14', '2021-01-29 22:12:32', 1, 1, 0);

-- ----------------------------
-- Table structure for videos
-- ----------------------------
DROP TABLE IF EXISTS `videos`;
CREATE TABLE `videos`  (
  `id` mediumint(255) NOT NULL AUTO_INCREMENT COMMENT '顺序ID',
  `name` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '视频详细名称',
  `vid` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT '视频唯一VID',
  `isdel` int(11) NOT NULL DEFAULT 0 COMMENT '是否已被删除',
  `fenlei` mediumtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '已被分类为',
  `watch` int(11) NOT NULL DEFAULT 0 COMMENT '观看视频次数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '视频库数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of videos
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
